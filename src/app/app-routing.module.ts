import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'first-page',
    loadChildren: () =>
      import('./pages/first-page/first-page.module').then(
        (m) => m.FirstPagePageModule
      ),
  },
  {
    path: '',
    redirectTo: 'first-page',
    pathMatch: 'full',
  },
  {
    path: 'second-page',
    loadChildren: () =>
      import('./pages/second-page/second-page.module').then(
        (m) => m.SecondPagePageModule
      ),
  },
  {
    path: 'third-page',
    loadChildren: () =>
      import('./pages/third-page/third-page.module').then(
        (m) => m.ThirdPagePageModule
      ),
  },
  {
    path: 'show-info',
    loadChildren: () =>
      import('./pages/show-info/show-info.module').then(
        (m) => m.ShowInfoPageModule
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
