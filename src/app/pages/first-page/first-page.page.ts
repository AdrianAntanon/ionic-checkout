import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-first-page',
  templateUrl: './first-page.page.html',
  styleUrls: ['./first-page.page.scss'],
})
export class FirstPagePage implements OnInit {
  promotionalCode: string = '';
  email: string = '';
  userName: string = '';
  lastName: string = '';
  company: string = '';
  address: string = '';
  country: string = '';
  province: string = '';
  city: string = '';
  zipCode: number = 0;
  telephoneNumber: number = 0;

  nextPage() {
    const promotional_cod: string =
      this.promotionalCode === '' ? 'No discount' : this.promotionalCode;
    const companyName: string =
      this.company === '' ? 'Sin compañía' : this.company;

    let goBack: boolean = false;

    const validateEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(this.email);
    if (!validateEmail) {
      this.presentAlert('Introduce un correo válido, por favor.');
      goBack = true;
    }
    if (this.userName === '' || this.lastName === '') {
      this.presentAlert('Rellena nombre y apellido, por favor.');
      goBack = true;
    }
    if (
      this.country === '' ||
      this.province === '' ||
      this.city === '' ||
      this.zipCode === 0
    ) {
      this.presentAlert('Necesitamos una dirección a la que enviar.');
      goBack = true;
    }

    if (this.telephoneNumber.toString().length !== 9) {
      this.presentAlert('El número de teléfono tiene que tener 9 dígitos');
      goBack = true;
    }

    if (goBack) {
      return;
    }

    const userEmail: string = this.email;
    const name: string = `${this.userName} ${this.lastName}`;
    const sendAddress: string = `${this.country}, ${this.province} ${this.city}, ${this.zipCode}, ${this.telephoneNumber}`;
    const myCountry: string = this.country;
    const myProvince: string = this.province;

    let navigationExtras: NavigationExtras = {
      state: {
        promotional_cod,
        userEmail,
        name,
        sendAddress,
        companyName,
        myCountry,
        myProvince,
      },
    };
    this.router.navigate(['second-page'], navigationExtras);
  }

  constructor(private router: Router, private alertCtrl: AlertController) {}

  async presentAlert(errMessage: string) {
    let messageHeader = 'No puedes avanzar! ';

    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: messageHeader,
      subHeader: `Necesario rellenar formulario.`,
      message: errMessage,
      buttons: ['Confirmar'],
    });
    await alert.present();
  }

  ngOnInit() {}
}
