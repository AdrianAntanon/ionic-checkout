import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-second-page',
  templateUrl: './second-page.page.html',
  styleUrls: ['./second-page.page.scss'],
})
export class SecondPagePage implements OnInit {
  data: object;
  address: string = '';
  fullName: string = '';
  shipment: string = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController
  ) {
    this.route.queryParams.subscribe((params) => {
      if (router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state;
        this.address = this.router.getCurrentNavigation().extras.state.sendAddress;
        this.fullName = this.router.getCurrentNavigation().extras.state.name;
      }
    });
  }

  async presentAlert(errMessage: string) {
    let messageHeader = 'No puedes avanzar! ';

    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: messageHeader,
      subHeader: `Necesario rellenar formulario.`,
      message: errMessage,
      buttons: ['Confirmar'],
    });
    await alert.present();
  }

  // Esto lo he hecho para testear, con pasarle todo por los inputs lo tengo hecho

  nextPage() {
    if (this.shipment === '') {
      this.presentAlert('Elige método de envío, por favor.');
      return;
    }
    const shipment = this.shipment;
    let navigationExtras: NavigationExtras = {
      state: {
        ...this.data,
        shipment,
      },
    };
    this.router.navigate(['third-page'], navigationExtras);
  }

  ngOnInit() {}
}
