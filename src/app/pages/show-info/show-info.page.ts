import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-show-info',
  templateUrl: './show-info.page.html',
  styleUrls: ['./show-info.page.scss'],
})
export class ShowInfoPage implements OnInit {
  data: object;
  promotionalCode: string;
  userEmail: string;
  userName: string;
  address: string;
  company: string;
  country: string;
  province: string;
  shipment: string;
  payment: string;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe((params) => {
      if (router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state;
        this.promotionalCode = this.router.getCurrentNavigation().extras.state.promotional_cod;
        this.userEmail = this.router.getCurrentNavigation().extras.state.userEmail;
        this.userName = this.router.getCurrentNavigation().extras.state.name;
        this.address = this.router.getCurrentNavigation().extras.state.sendAddress;
        this.company = this.router.getCurrentNavigation().extras.state.companyName;
        this.country = this.router.getCurrentNavigation().extras.state.myCountry;
        this.province = this.router.getCurrentNavigation().extras.state.myProvince;
        this.shipment = this.router.getCurrentNavigation().extras.state.shipment;
        this.payment = this.router.getCurrentNavigation().extras.state.payment;

        console.log(this.data, 'página final');
      }
    });
  }

  ngOnInit() {}
}
