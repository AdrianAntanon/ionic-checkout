import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-third-page',
  templateUrl: './third-page.page.html',
  styleUrls: ['./third-page.page.scss'],
})
export class ThirdPagePage implements OnInit {
  data: object;
  payment: string = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController
  ) {
    this.route.queryParams.subscribe((params) => {
      if (router.getCurrentNavigation().extras.state) {
        this.data = {
          ...this.router.getCurrentNavigation().extras.state,
        };
      }
    });
  }

  async presentAlert(errMessage: string) {
    let messageHeader = 'No puedes avanzar! ';

    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: messageHeader,
      subHeader: `Necesario rellenar formulario.`,
      message: errMessage,
      buttons: ['Confirmar'],
    });
    await alert.present();
  }

  nextPage() {
    if (this.payment === '') {
      this.presentAlert('Elige método de pago, por favor');
      return;
    }
    const payment: string = this.payment;
    let navigationExtras: NavigationExtras = {
      state: {
        ...this.data,
        payment,
      },
    };
    this.router.navigate(['show-info'], navigationExtras);
  }

  ngOnInit() {}
}
