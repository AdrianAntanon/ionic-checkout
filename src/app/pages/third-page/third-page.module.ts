import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ThirdPagePageRoutingModule } from './third-page-routing.module';

import { ThirdPagePage } from './third-page.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ThirdPagePageRoutingModule,
    ComponentsModule,
  ],
  declarations: [ThirdPagePage],
})
export class ThirdPagePageModule {}
