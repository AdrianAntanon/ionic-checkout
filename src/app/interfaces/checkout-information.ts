export interface CheckoutInformation {
  promotionalCode: string;
  email: string;
  userName: string;
  lastName: string;
  company: string;
  address: string;
  country: string;
  province: string;
  city: string;
  zipCode: number;
  telephoneNumber: number;
}
